#!/bin/sh
#
# Implicit solvent Amber MD simulation for peptides or proteins
#
# Input: 
#     mol.pdb 
#
# Last updated: 
#        24 July 2015 -- writing out mcdrd trajectories in order to import into MOE
#        19 July 2015 -- tested on Fv 
#        10 July 2015 
##########################################################################

## TOSET 
# 0, 2, 3, 4 on gpuk40 
# 0 on rhel11 for titan card 
export CUDA_VISIBLE_DEVICES=0   
AMBERHOME=/apps/pkgs/AMBER/amber14.20150922
#AMBERHOME=/clu01/applic/amber14

###########################################
#          LEAP 
###########################################
if [ 1 -eq 1 ] 
then 
rm -f leap.in leap.log prot.prmtop prot.inpcrd
cat > leap.in <<EOF

source $AMBERHOME/dat/leap/cmd/leaprc.ff14SBonlysc
prot = loadpdb mol.pdb 
set default PBRadii mbondi3
saveamberparm prot prot.prmtop prot.inpcrd 
savepdb prot prot.pdb 
quit

EOF

echo leap 
$AMBERHOME/AmberTools/src/leap/tleap -f leap.in 

fi 

###########################################
#       MIN
##########################################
if [ 1 -eq 1 ] 
then 
rm mdinfo min.in prot_min.rst prot_min.pdb min.out 
cat > min.in <<EOF
Complex: initial protein minimization
 &cntrl
  imin = 1,
  ntpr = 25,
  ntb = 0, cut = 999,
  ntmin = 1, maxcyc = 4000, ncyc = 200,
  igb = 8
/
EOF

echo min
$AMBERHOME/bin/pmemd.cuda -O -i min.in -o min.out -p prot.prmtop -c prot.inpcrd -r prot_min.rst
$AMBERHOME/bin/ambpdb -p prot.prmtop < prot_min.rst > prot_min.pdb 

fi 

###########################################
#      EQUI  
##########################################
if [ 1 -eq 1 ] 
then 
rm -f mdinfo equi.in  

# ntc = 1 for noshake; 2 for shake
# ntf = 2 if ntc = 2
# dt=0.002 with shake; 1fs otherwise 
cat > equi.in << EOF
GB equil 100ps 
 &cntrl
  imin=0,irest=0,
  nstlim=100000,
  dt=0.001,
  igb=8,
  ntc=1,ntf=1,ig=-1,
  cut=999., ntb=0,
  ntpr=10000, ntwx=10000, ntwr=1000000,
  ntt=3, gamma_ln=2.0,
  temp0=300.0,
 /
 /
EOF

echo equi 
$AMBERHOME/bin/pmemd.cuda -O -i equi.in -o equi.log -x prot_equi.mdcrd -p prot.prmtop -c prot_min.rst -r prot_equi.rst
$AMBERHOME/bin/ambpdb -p prot.prmtop < prot_equi.rst > prot_equi.pdb

fi 

###########################################
#      PRODUCTION 
##########################################
if [ 1 -eq 1 ] 
then 
rm -f mdinfo md-prod.in  

# ntc = 1 for noshake; 2 for shake 
# ntf = 2 if ntc = 2 
# dt=0.002 with shake 
# ntxo = 2 --> NetCDF output (rst)
# ioutfm = 1 --> NetCDF output (trj)
# ig = -1 --> seed base on date and hours, only if not a restart
# nscm=1000,   (remove rot & trans) 
cat > md-prod.in << EOF 
GB PROD MD 10ns
 &cntrl
  imin=0,irest=1,ntx=5,
  nstlim=5000000,
  dt=0.002,
  igb=8, ig = -1,
  ntc=2,ntf=2,
  cut=999., ntb=0,
  nscm=1000,
  ntpr=50000, ntwx=50000, ntwr=500000,
  ntt=3, gamma_ln=1.0,
  temp0=300.0,
 /
 /
EOF

echo md
$AMBERHOME/bin/pmemd.cuda -O -i md-prod.in -o md-prod.log -x prot_md.mdcrd   -p prot.prmtop -c prot_equi.rst -r prot_md.rst 
# $AMBERHOME/bin/ambpdb -p prot.prmtop < prot_md.rst > prot_md.pdb

fi 



