Post-Translation Modification Risk Report
=========================================
Filename : output_m71derived/seq_0002_c17m159_hc_c17m159_lc_ptm.txt 
Date Created (YY/MM/DD): 2016/1/25 
 
Light  chain: 
EIVLTQSPATLSLSPGERATLSCRASQSVSDFLAWYQQKPGQAPRLLIYD  
ASNRATGIPARFSGSGSGTDFTLTISSLEPEDFAVYYCQQGISAPLTFGQGTKVEIK  
.........1.........2.........3.........4.........5  
Heavy  chain :   
QVQLVQSGAEVKKPGSSVKVSCKASGGTFSDYWISWVRQAPGQGLEWMGD  
IYPTSGTADYAQKFQGRVTITADESTSTAYMELSSLRSEDTAVYYCARESWSELDYWGQGTLVTVS  
.........1.........2.........3.........4.........5  
 
------------- CDRs ----------------- 
  CDR definition = CCG 
  Number of flanking residues = 1 
    (residue numbers exclude flanking residues) 

CDR_H1  26 - 35  : S  GGTFSDYWIS  W  
CDR_H2  50 - 66  : G  DIYPTSGTADYAQKFQG  R  
CDR_H3  99 - 106 : R  ESWSELDY  W  
CDR_L1  24 - 34  : C  RASQSVSDFLA  W  
CDR_L2  50 - 56  : Y  DASNRAT  G  
CDR_L3  89 - 97  : C  QQGISAPLT  F  



----------------PTM Motif Analysis in CDRs-------------------------

  NR : found at residue 53 in chain VL
    single structure    Abs SASA ASN 53 = 72.30 
    single structure    %   SASA ASN 53 = 45.67 
    hbond TYR 49:N -- ASN 53:O 
    #atoms within 4 Ang = 24
    deamidation thalf = 4.07 days, pentapeptide thalf = 59.70 days,  screening factor = 0.0682096 
  W : found at residue 33 in chain VH
    single structure    Abs SASA TRP 33 = 135.16 
    single structure    %   SASA TRP 33 = 48.28 
    hbond GLU 99:N -- TRP 33:O 
    #atoms within 4 Ang = 28
  W : found at residue 101 in chain VH
    single structure    Abs SASA TRP 101 = 164.49 
    single structure    %   SASA TRP 101 = 58.75 
    No hbonds found
    #atoms within 4 Ang = 17

----------------PTM Motif Analysis in Framework-------------------------

  W : found at residue 35 in chain VL
    single structure    Abs SASA TRP 35 = 0.00 
    single structure    %   SASA TRP 35 = 0.00 
    hbond ILE 48:N -- TRP 35:O 
    #atoms within 4 Ang = 37
  C : found at residue 23 in chain VL
    single structure    Abs SASA CYS 23 = 0.00 
    single structure    %   SASA CYS 23 = 0.00 
    hbond GLN 90:NE2 -- CYS 23:SG 
    #atoms within 4 Ang = 27
  C : found at residue 88 in chain VL
    single structure    Abs SASA CYS 88 = 0.00 
    single structure    %   SASA CYS 88 = 0.00 
    hbond GLY 99:CA -- CYS 88:SG 
    #atoms within 4 Ang = 25
  W : found at residue 36 in chain VH
    single structure    Abs SASA TRP 36 = 0.00 
    single structure    %   SASA TRP 36 = 0.00 
    hbond MET 48:N -- TRP 36:O 
    #atoms within 4 Ang = 39
  W : found at residue 47 in chain VH
    single structure    Abs SASA TRP 47 = 0.00 
    single structure    %   SASA TRP 47 = 0.00 
    No hbonds found
    #atoms within 4 Ang = 31
  W : found at residue 107 in chain VH
    single structure    Abs SASA TRP 107 = 5.33 
    single structure    %   SASA TRP 107 = 1.90 
    No hbonds found
    #atoms within 4 Ang = 22
  M : found at residue 48 in chain VH
    single structure    Abs SASA MET 48 = 0.00 
    single structure    %   SASA MET 48 = 0.00 
    hbond ALA 61:N -- MET 48:O 
    #atoms within 4 Ang = 23
  M : found at residue 81 in chain VH
    single structure    Abs SASA MET 81 = 0.00 
    single structure    %   SASA MET 81 = 0.00 
    hbond VAL 20:N -- MET 81:O 
    #atoms within 4 Ang = 30
  C : found at residue 22 in chain VH
    single structure    Abs SASA CYS 22 = 0.00 
    single structure    %   SASA CYS 22 = 0.00 
    hbond ALA 79:N -- CYS 22:O 
    #atoms within 4 Ang = 25
  C : found at residue 96 in chain VH
    single structure    Abs SASA CYS 96 = 0.00 
    single structure    %   SASA CYS 96 = 0.00 
    hbond GLY 108:CA -- CYS 96:SG 
    #atoms within 4 Ang = 26

-----------------------------------Risk Assignment Rules Begin--------------------------------------
Deamindation 
  NG    : %SASA> 30 => high, medium othersiwe 
  NH, NS: %SASA> 50 => high, medium othersiwe 
  others: halflife<10days and %SASA>50  => medium, low otherwise

Oxidation W, M, C
     %SASA < 20 => low 
 20< %SASA < 50 => medium 
  >  %SASA      => high  
  
DG 
  exp >30 %  => high  
  exp <30 %  => med 
DP 
  in H3 & >50% => med 
  not in H3  => low 
DS 
  always low   
-----------------------------------Risk Assignment Rules End--------------------------------------


---------------------------PTM SUMMARY--------------------------------------------------------
PTMTYPE   CDRLabel    MOTIF      RESIDUE    SASA      %SASA     #Neighbors   Half Life      RISK 
----------------------------------------------------------------------------------------------
deamid    CDR_L2      NR         ASN53        72.3      45.7        24			  4.07      low 
oxida     CDR_H1      W          TRP33       135.2      48.3        28			   -        med 
oxida     CDR_H3      W          TRP101      164.5      58.8        17			   -        hi  
oxida     FR_VL       W          TRP35         0.0       0.0        37			   -        low 
cys       FR_VL       C          CYS23         0.0       0.0        27			   -        low 
cys       FR_VL       C          CYS88         0.0       0.0        25			   -        low 
oxida     FR_VH       W          TRP36         0.0       0.0        39			   -        low 
oxida     FR_VH       W          TRP47         0.0       0.0        31			   -        low 
oxida     FR_VH       W          TRP107        5.3       1.9        22			   -        low 
oxida     FR_VH       M          MET48         0.0       0.0        23			   -        low 
oxida     FR_VH       M          MET81         0.0       0.0        30			   -        low 
cys       FR_VH       C          CYS22         0.0       0.0        25			   -        low 
cys       FR_VH       C          CYS96         0.0       0.0        26			   -        low 
-------------------------------------------------------------------------------------------------
 Note: half life of Asparagine deamidation is in days; all framework risks default to low.